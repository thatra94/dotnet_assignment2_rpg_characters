# Dotnet_assignment2_rpg_characters

Dotnet assignment for Experis programme at Noroff. 

The purpose of the project is to demonstrate modelling and structuring skills using SOLID prinsiples with design patterns and interfaces.

The design patterns that are used in this project are Strategy and Factory. 

The functionality of the program is demonstrated by using tests. So to check if the program is working run all tests. 

## Assignment Requirements

### Optional

**Testing**
- [x] Demonstrate the function of the application with the use of tests.
Implemented with Xunit and reached a code coverage of **87%**. 

Coverage report can be viewed at `rpg_characters_tests/coveragereport/index.html` or opening the screenshot at `images/coverage_test_image`



### Characters

**Attributes**
- [x] Health, how much health they have
- [x] Strength, 1 point results in 1.5 additional melee weapon damage
- [x] Dexterity, 1 point results in 2 additional ranged weapon damage
- [x] Intelligence, 1 point results in 2 additional ranged weapon damage
- [x] Level, start at level 1, 100 experience needed for level 2 and experience needed for next level increses by 10% each time

**Item slots**
- [x] Weapon
- [x] Head
- [x] Body
- [x] Legs

### Character classes

**Warrior**
- [x] At level 1, 150 HP, 10 strength, 3 dexterity, 1 intelligence
- [x] On level up 30 HP, 5 strength, 2 dexterity, 1 intelligence

**Ranger**
- [x] At level 1, 120 HP, 5 strength, 10 dexterity, 2 intelligence
- [x] On level up, 20 HP, 2 strength, 5 dexterity, 1 intelligence

**Mage**
- [x] At level 1, 100 HP, 2 strength, 3 dexterity, 10 intelligence
- [x] On level up, 15 HP, 1 strength, 2 dexterity, 5 intelligence

### Items
- [x] All items scale from level 1, base + stats for level 1 (base + level*scale)

**Weapons** 

- [x] Melee weapons, base damage 15, scaling by 2 each level. 
- [x] Melee weapons, damage dealt is base damage(+scaling) + character strength value * 1.5 rounded down


- [x] Ranged weapons, base damage 5, scaling by 3 each level
- [x] Ranged weapons, damage dealt is base damage(+scaling) + character dexterity value * 2 rounded down


- [x] Magic weapons, base damage 25, scaling by 2 each level
- [x] Magic weapons, damage dealt is base damage(+scaling) + character intelligence value * 3 rounded down


- [x] If a character has no weapon equipped, this should result in no damage dealt

**Armor** 

- [x] Scaling is dependent on what slot they are in(calculated after all bonuses)
- [x] Body is 100%
- [x] Head is 80%
- [x] Legs are 60%


- [x] Cloth armor, base bonus 10 HP, 3 intelligence, 1 dexterity
- [x] Cloth armor, scaled with 5 HP, 2 intelligence, 1 dexterity per level


- [x] Leather armor, base bonus of 20 HP, 3 dexterity, 1 strength 
- [x] Leather armor, scaled with 8 HP, 2 dexterity, 1 strength per level


- [x] Plate armor, base bonus of 30 HP, 3 strength, 1 dexterity
- [x] Plate armor, scaled with 12 Hp, 2 strength, 1 dexterity per level


- [x] When a character equips item their stats should update accordingly. Effective stats = character stats + stats from gear
- [x] Should replace stats from existing equipment when changing gear in same slots
