﻿using rpg_characters.Stats;

namespace rpg_characters.Heroes.HeroStrategyClasses
{
    public class Mage : IHero
    {
        public BaseStats BaseStats { get; set; }

        // Starting stats for Mage
        public Mage(BaseStats baseStats)
        {
            BaseStats = baseStats;
            BaseStats.health = 100;
            BaseStats.strength = 2;
            BaseStats.dexterity = 3;
            BaseStats.intelligence = 10;

        }

        // Stats gained per level for Mage
        public void LevelUp()
        {
            BaseStats.level += 1;
            BaseStats.health += 15;
            BaseStats.strength += 1;
            BaseStats.dexterity += 2;
            BaseStats.intelligence += 5;
        }

    }
}
