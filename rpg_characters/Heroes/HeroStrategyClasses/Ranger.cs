﻿using rpg_characters.Stats;

namespace rpg_characters.Heroes.HeroStrategyClasses
{
    public class Ranger : IHero
    {
        public BaseStats BaseStats { get; set; }

        // Starting stats for Ranger
        public Ranger(BaseStats baseStats)
        {
            BaseStats = baseStats;
            BaseStats.health = 120;
            BaseStats.strength = 5;
            BaseStats.dexterity = 10;
            BaseStats.intelligence = 2;

        }
        // Stats gained per level for Ranger
        public void LevelUp()
        {
            BaseStats.level += 1;
            BaseStats.health += 20;
            BaseStats.strength += 2;
            BaseStats.dexterity += 5;
            BaseStats.intelligence += 1;
        }

    }
}

