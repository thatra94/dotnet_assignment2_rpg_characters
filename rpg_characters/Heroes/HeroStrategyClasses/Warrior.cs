﻿using rpg_characters.Stats;

namespace rpg_characters.Heroes.HeroStrategyClasses
{
    public class Warrior : IHero
    {

        public BaseStats BaseStats { get; set; }

        // Starting stats for Warrior
        public Warrior(BaseStats baseStats)
        {
            BaseStats = baseStats;
            BaseStats.health = 150;
            BaseStats.strength = 10;
            BaseStats.dexterity = 3;
            BaseStats.intelligence = 1;

        }

        // Stats gained per level for Warrior
        public void LevelUp()
        {
            BaseStats.level += 1;
            BaseStats.health += 30;
            BaseStats.strength += 5;
            BaseStats.dexterity += 2;
            BaseStats.intelligence += 1;
        }
    }
}
