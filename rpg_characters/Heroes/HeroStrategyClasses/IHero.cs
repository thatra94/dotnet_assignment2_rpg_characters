﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Heroes.HeroStrategyClasses
{
    public interface IHero
    {

        public BaseStats BaseStats { get; set; }

        public void LevelUp(){}
    }
}
