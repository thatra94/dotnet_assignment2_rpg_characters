﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Heroes.HeroStrategyClasses
{

    public interface IHeroStatCalculations
    {
        public BaseStats UpdateStatsFromArmor(BaseStats EffectiveStats, BaseStats armorStats) {
            return EffectiveStats;
        }

        public int CalculateNextLevelExperience(int experience)
        {
            return experience;
        }

        public int CalculateExperienceInNextLevel(int experience, int currentLevelExperience)
        {
            return experience;
        }
    }
}
