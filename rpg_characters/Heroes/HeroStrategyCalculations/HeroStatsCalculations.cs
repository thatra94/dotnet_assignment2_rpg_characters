﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Heroes.HeroStrategyClasses
{
    // Implements the interface to be able to polymporph into that interface in Hero
    public class HeroStatsCalculations : IHeroStatCalculations
    {
        // takes in current effectiveStats and adds stats from armor
        public BaseStats UpdateStatsFromArmor(BaseStats effectiveStats, BaseStats armorStats)
        {
            effectiveStats.health += armorStats.health;
            effectiveStats.strength += armorStats.strength;
            effectiveStats.dexterity += armorStats.dexterity;
            effectiveStats.intelligence += armorStats.intelligence;
            return effectiveStats;
        }

        // adds the increased experience needed per level and also casts it to int
        public int CalculateNextLevelExperience(int experience) {
            return (int)(experience * 1.1);
        }
        public int CalculateExperienceInNextLevel(int experience, int currentLevelExperience)
        {
            return experience - currentLevelExperience;
        }

    }
}
