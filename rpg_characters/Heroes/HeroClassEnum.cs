﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Heroes
{
    public class HeroClassEnum
    {

        // Enum for the different available classes
        public enum HeroClass
        {
            Warrior,
            Ranger,
            Mage
        }
    }
}
