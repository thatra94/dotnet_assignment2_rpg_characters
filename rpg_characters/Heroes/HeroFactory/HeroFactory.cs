﻿using rpg_characters.Heroes.HeroStrategyClasses;
using rpg_characters.Stats;
using System;

namespace rpg_characters.Heroes
{
    public class HeroFactory
    {
        // Herofactory to create a hero, only need to specify what class and what kind of stat calculations that hero is gonna have. 
        public static Hero CreateHero(HeroClassEnum.HeroClass heroClass, IHeroStatCalculations heroStatCalculations)
        {
            Hero hero;
            BaseStats baseStats = new BaseStats();
            BaseStats effectiveStats = new BaseStats();
            switch (heroClass)
            {
                case HeroClassEnum.HeroClass.Warrior:
                    Warrior warrior = new Warrior(baseStats);
                    hero = new Hero(warrior, effectiveStats, heroStatCalculations);
                    break;
                case HeroClassEnum.HeroClass.Ranger:
                    Ranger ranger = new Ranger(baseStats);
                    hero = new Hero(ranger, effectiveStats, heroStatCalculations);
                    break;
                case HeroClassEnum.HeroClass.Mage:
                    Mage mage = new Mage(baseStats);
                    hero = new Hero(mage, effectiveStats, heroStatCalculations);
                    break;
                default:
                    throw new ArgumentException();
            }
            return hero;

        }
    }
}
