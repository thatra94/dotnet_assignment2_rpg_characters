﻿using rpg_characters.Equipments;
using rpg_characters.Equipments.Armors.ArmorContext;
using rpg_characters.Equipments.Weapons;
using rpg_characters.Heroes.HeroStrategyClasses;
using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Heroes
{
     public class Hero 
    {
    
        // Because of inheritance all the different slots of armor is an armor. 
        public Armor HeadArmor { get; set; }
        public Armor BodyArmor { get; set; }
        public Armor LegArmor { get; set; }
        public Weapon WeaponEquipment { get; set; }
        public int AttackDamage { get; set; }
        // By using a Interface IHero can be Mage, Ranger or Warrior
        public IHero HeroClass { get; set; }
        // By having calculations in another class with interface we can easily add different calculations in the future
        // We also seperate funcitonality to make the classes only have 1 responsibility
        public IHeroStatCalculations HeroStatCalculations { get; set; }
        // Making a basestats class because we are using the same properties alot
        public BaseStats EffectiveStats { get; set; }
        public int ExperienceNeededToLevel { get; set; }
        public int CurrentLevelExperience { get; set; }

        public Hero(IHero heroClass, BaseStats effectiveStats, IHeroStatCalculations heroStatCalculations)
        {
            ExperienceNeededToLevel = 100;
            CurrentLevelExperience = 0;
            EffectiveStats = effectiveStats;
            HeroClass = heroClass;
            HeroStatCalculations = heroStatCalculations;
        }

        // Adds experience to the hero, calls the level up and calculate next level experience if it exceeds experience needed to lvl
        // Also calls the herostatcalculations strategy to calculate current experience
        public void addExperience(int experience)
        {
            while(experience + CurrentLevelExperience >= ExperienceNeededToLevel)
            {
                CurrentLevelExperience = HeroStatCalculations.CalculateExperienceInNextLevel(experience, CurrentLevelExperience);
                experience = experience - ExperienceNeededToLevel;
                LevelUp();
                calculateNextLevelExperience();
  
            }
            CurrentLevelExperience += experience;

        }

        // Calls the heroclasses' level up function, as different classes have different scaling on level up
        public void LevelUp()
        {
            HeroClass.LevelUp();
        }

        // calls the herostatcalculations strategy to calculate next level experience
        public void calculateNextLevelExperience()
        {
            ExperienceNeededToLevel = HeroStatCalculations.CalculateNextLevelExperience(ExperienceNeededToLevel);
        }

        // Adds weapon and calculates effective attack damage
        public void AddWeapon(Weapon weapon)
        {
            WeaponEquipment = weapon;
            calculateAttackDamage();
        }

        // Calls the different weapons' calculation of attackdamage, depending on the stats they scale to. 
        public void calculateAttackDamage()
        {
            AttackDamage = WeaponEquipment.CalculateAttackDamage(HeroClass.BaseStats);
        }

        public void RemoveWeapon()
        {
            WeaponEquipment = null;
        }

        //Takes in a armor type and sets it in the appropiate slot
        public void addEquipment(Armor armor)
        {
            EquipmentEnum.ArmorSlots slot = armor.EquipmentSlot;
                
            switch(slot)
            {
                case EquipmentEnum.ArmorSlots.Head:
                    this.HeadArmor = armor;
                    break;
                case EquipmentEnum.ArmorSlots.Body:
                    this.BodyArmor = armor;
                    break;
                case EquipmentEnum.ArmorSlots.Legs:
                    this.LegArmor = armor;
                    break;
                default:
                    throw new ArgumentException();
            }
            updateHeroStats();
        }

        // Takes in a slot and removes the equipment from that slot
        public void RemoveEquipment(EquipmentEnum.ArmorSlots slot)
        {
            switch (slot)
            {
                case EquipmentEnum.ArmorSlots.Head:
                    this.HeadArmor = null;
                    break;
                case EquipmentEnum.ArmorSlots.Body:
                    this.BodyArmor = null;
                    break;
                case EquipmentEnum.ArmorSlots.Legs:
                    this.LegArmor = null;
                    break;
                default:
                    throw new ArgumentException();
            }
            updateHeroStats();

        }

        // Adds stats from armor if there is a armor in that slot and adds it to effectiveStats
        public void updateHeroStats()
        {

            setBaseStatsFromHeroClass();

            if (HeadArmor != null)
            {
                EffectiveStats = HeroStatCalculations.UpdateStatsFromArmor(EffectiveStats, HeadArmor.BaseStats);
            }
            if (BodyArmor != null)
            {
                EffectiveStats = HeroStatCalculations.UpdateStatsFromArmor(EffectiveStats, BodyArmor.BaseStats);
            }
            if (LegArmor != null)
            {
                EffectiveStats = HeroStatCalculations.UpdateStatsFromArmor(EffectiveStats, LegArmor.BaseStats);
            }
        }

        private void setBaseStatsFromHeroClass()
        {
            EffectiveStats.health = HeroClass.BaseStats.health;
            EffectiveStats.strength = HeroClass.BaseStats.strength;
            EffectiveStats.dexterity = HeroClass.BaseStats.dexterity;
            EffectiveStats.intelligence = HeroClass.BaseStats.intelligence;
        }
    }
}
