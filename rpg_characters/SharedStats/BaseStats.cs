﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Stats
{

    // Basestats properties that are used often by other classes
    public class BaseStats
    {

        public int health { get; set; }
        public int strength { get; set; }
        public int dexterity { get; set; }
        public int intelligence { get; set; }
        public int level { get; set; }


        public BaseStats()
        {
            health = 0;
            strength = 0;
            dexterity = 0;
            intelligence = 0;
            level = 1;
        }

    }
}
