﻿using rpg_characters.Equipments.Armors.ArmorContext;
using rpg_characters.Equipments.Armors.LevelScaleStrategy;
using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Armors.ArmorFactoryClient
{
    public class ArmorFactory
    {
        // Armor factory that creates armor and also scales the armor depending on their slots and level
        public static Armor CreateArmor(string name, int level, EquipmentEnum.ArmorTypes equipmentType, EquipmentEnum.ArmorSlots equipmentSlot, IScaleArmorSlot scaleArmor)
        {

            Armor armor;
            switch (equipmentType)
            {
                // Does not directly create a armor, because armor does not have the function calculateArmorLevelStats
                case EquipmentEnum.ArmorTypes.Plate:
                    PlateArmor plateArmor = new PlateArmor(name, level, equipmentSlot, scaleArmor, new BaseStats());
                    plateArmor.CalculateArmorLevelStats();
                    armor = plateArmor;
                    break;
                case EquipmentEnum.ArmorTypes.Leather:
                    LeatherArmor leatherArmor = new LeatherArmor(name, level, equipmentSlot, scaleArmor, new BaseStats());
                    leatherArmor.CalculateArmorLevelStats();
                    armor = leatherArmor;
                    break;
                case EquipmentEnum.ArmorTypes.Cloth:
                    ClothArmor clothArmor = new ClothArmor(name, level, equipmentSlot, scaleArmor, new BaseStats());
                    clothArmor.CalculateArmorLevelStats();
                    armor = clothArmor;
                    break;

                default:
                    throw new ArgumentException();
            }
            armor.CalculateArmorSlotScale();
            return armor;

        }
    }

}

