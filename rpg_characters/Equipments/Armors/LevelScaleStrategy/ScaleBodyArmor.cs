﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Armors.LevelScaleStrategy
{
    // Body armor has 100% stats and requires no scaling, there only returning the basestats
    public class ScaleBodyArmor : IScaleArmorSlot
    {
        public BaseStats CalculateArmorScale(BaseStats baseStats)
        {
            return baseStats;
        }
    }
}
