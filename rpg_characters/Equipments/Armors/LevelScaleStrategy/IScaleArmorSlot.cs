﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Armors.LevelScaleStrategy
{
    // Interface for armor scaling depending on what slot they are in
    public interface IScaleArmorSlot
    {
        public BaseStats CalculateArmorScale(BaseStats baseStats) {
            return baseStats;
       }
    }
}
