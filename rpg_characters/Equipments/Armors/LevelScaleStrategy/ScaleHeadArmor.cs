﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Armors.LevelScaleStrategy
{
    public class ScaleHeadArmor : IScaleArmorSlot
    {
        // Head armor has only 80% of stats, so it is multiplied by 0.8. Also casts the stats to int after calculation
        public BaseStats CalculateArmorScale(BaseStats baseStats)
        {
            baseStats.health = (int)(baseStats.health * 0.8);
            baseStats.strength = (int)(baseStats.strength * 0.8);
            baseStats.dexterity = (int)(baseStats.dexterity * 0.8);
            baseStats.intelligence = (int)(baseStats.intelligence * 0.8);

            return baseStats;
        }
    }
}
