﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Armors.LevelScaleStrategy
{
    public class ScaleLegArmor : IScaleArmorSlot
    {

        // Leg armor has only 60% of stats, so it is multiplied by 0.6. Also casts the stats to int after calculation

        public BaseStats CalculateArmorScale(BaseStats baseStats)
        {
            baseStats.health = (int)(baseStats.health * 0.6);
            baseStats.strength = (int)(baseStats.strength * 0.6);
            baseStats.dexterity = (int)(baseStats.dexterity * 0.6);
            baseStats.intelligence = (int)(baseStats.intelligence * 0.6);

            return baseStats;
        }
    }
}
