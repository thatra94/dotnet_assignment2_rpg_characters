﻿using rpg_characters.Equipments.Armors.LevelScaleStrategy;
using rpg_characters.Stats;


namespace rpg_characters.Equipments.Armors.ArmorContext
{
    // Implements the abstract class armor to directly inherit all its properties and functions and also be able to polymoprh into Armor 
    public class PlateArmor : Armor
    {
        public PlateArmor(string armorName, int level, EquipmentEnum.ArmorSlots slot, IScaleArmorSlot scaleArmor,  BaseStats baseStats) : base(armorName, level, slot, scaleArmor, baseStats)
        {
            BaseStats.health = 30;
            BaseStats.strength = 3;
            BaseStats.dexterity = 1;
            BaseStats.intelligence = 0;
        }

        // Calculates the armor stats depending on armor level
        public void CalculateArmorLevelStats ()
        {
            BaseStats.health = BaseStats.health + (12 * Level);
            BaseStats.strength = BaseStats.strength + (2 * Level);
            BaseStats.dexterity = BaseStats.dexterity + (1 * Level);
            BaseStats.intelligence = 0;
        }
    }
}
