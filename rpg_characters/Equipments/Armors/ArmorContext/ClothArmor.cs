﻿using rpg_characters.Equipments.Armors.LevelScaleStrategy;
using rpg_characters.Stats;

namespace rpg_characters.Equipments.Armors.ArmorContext
{
    // Implements the abstract class armor to directly inherit all its properties and functions and also be able to polymoprh into Armor 
    public class ClothArmor : Armor
    {
        public ClothArmor(string armorName, int level, EquipmentEnum.ArmorSlots slot, IScaleArmorSlot scaleArmor, BaseStats baseStats) : base(armorName, level, slot, scaleArmor, baseStats)
        {
            BaseStats.health = 10;
            BaseStats.strength = 0;
            BaseStats.dexterity = 1;
            BaseStats.intelligence = 3;
        }

        // Calculates the armor stats depending on armor level
        public void CalculateArmorLevelStats()
        {
            BaseStats.health = BaseStats.health + (5 * Level);
            BaseStats.strength = 0;
            BaseStats.dexterity = BaseStats.dexterity + (1 * Level);
            BaseStats.intelligence = BaseStats.intelligence + (2 * Level);
        }
    }
}
