﻿using rpg_characters.Equipments.Armors.LevelScaleStrategy;
using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Armors.ArmorContext
{
    // Implements the abstract class armor to directly inherit all its properties and functions and also be able to polymoprh into Armor 
    public class LeatherArmor : Armor
    {
        public LeatherArmor(string armorName, int level, EquipmentEnum.ArmorSlots slot, IScaleArmorSlot scaleArmor, BaseStats baseStats) : base(armorName, level, slot, scaleArmor, baseStats)
        {
            BaseStats.health = 20;
            BaseStats.strength = 1;
            BaseStats.dexterity = 3;
            BaseStats.intelligence = 0;
        }

        // Calculates the armor stats depending on armor level
        public void CalculateArmorLevelStats()
        {
            BaseStats.health = BaseStats.health + (8 * Level);
            BaseStats.strength = BaseStats.strength + (1 * Level);
            BaseStats.dexterity = BaseStats.dexterity + (2 * Level);
            BaseStats.intelligence = 0;
        }
    }
}
