﻿using rpg_characters.Equipments.Armors.LevelScaleStrategy;
using rpg_characters.Stats;


namespace rpg_characters.Equipments.Armors.ArmorContext
{
    public abstract class Armor
    {
        public string ArmorName { get; set; }

        public int Level { get; set; }

        public BaseStats BaseStats { get; set; }
        public EquipmentEnum.ArmorSlots EquipmentSlot { get; set; }
        // Takes in a strategy for scaling depending on armor slot
        public IScaleArmorSlot ScaleArmor { get; set; }

        public Armor(string armorName, int level, EquipmentEnum.ArmorSlots slot, IScaleArmorSlot scaleArmor, BaseStats baseStats)
        {
            ArmorName = armorName;
            Level = level;
            EquipmentSlot = slot;
            ScaleArmor = scaleArmor;
            BaseStats = baseStats;
        }

        // Calls the different slots armor scale depending on what kind of slots that armor is in
        public void CalculateArmorSlotScale()
        {
            ScaleArmor.CalculateArmorScale(BaseStats);
        }
    }
}
