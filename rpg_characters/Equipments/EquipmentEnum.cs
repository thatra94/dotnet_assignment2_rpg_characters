﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments
{
    public class EquipmentEnum
    {
        // Enum for different slots armor can go
        public enum ArmorSlots
        {
            Head, Body, Legs
        }

        // Enum for diffrent armor types
        public enum ArmorTypes
        {
            Plate, Leather, Cloth
        }

        // Enum for different weapon types
        public enum WeaponTypes
        {
            MeleeWeapon, RangedWeapon, MagicWeapon
        }

    }
}
