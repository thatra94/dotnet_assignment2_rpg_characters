﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Weapons
{
    public class WeaponFactory
    {
        // Weaponfactory that creates weapon with parameters weapontype, name and weapon lvl. 
        public static Weapon CreateWeapon(EquipmentEnum.WeaponTypes weaponType, string name, int level)
        {
            Weapon weapon;
            switch (weaponType)
            {
                case EquipmentEnum.WeaponTypes.MeleeWeapon:
                    MeleeWeapon meleeWeapon = new MeleeWeapon(name, level);
                    weapon = new Weapon(meleeWeapon);
                    break;
                case EquipmentEnum.WeaponTypes.RangedWeapon:
                    RangedWeapon rangedWeapon = new RangedWeapon(name, level);
                    weapon = new Weapon(rangedWeapon);
                    break;
                case EquipmentEnum.WeaponTypes.MagicWeapon:
                    MagicWeapon magicWeapon = new MagicWeapon(name, level);
                    weapon = new Weapon(magicWeapon);
                    break;
                default:
                    throw new ArgumentException();
            }
            weapon.CalculateBaseDamage();
            return weapon;
        }
    }
}

