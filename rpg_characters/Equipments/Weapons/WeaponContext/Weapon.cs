﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Weapons
{
    public class Weapon
    {

        // Takes a IWeapon type to be able to take in any weapon that implements that interface
        public IWeapon WeaponType { get; set; }

        public Weapon(IWeapon weaponType)
        {
            WeaponType = weaponType;
        }

        //Able to call the specific weapons functions for calculations
        public int CalculateAttackDamage(BaseStats stats)
        {
            return WeaponType.CalculateAttackDamage(stats);
        }
        public void CalculateBaseDamage()
        {
            WeaponType.CalculateBaseDamage();
        }
    }
}
