﻿using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments.Weapons
{
    
    public interface IWeapon {
        public string Name { get; set; }
        public int Level { get; set; }
        public int BaseDamage { get; set; }
        public int CalculateAttackDamage(BaseStats stats);
        public void CalculateBaseDamage(){}
    }
}
