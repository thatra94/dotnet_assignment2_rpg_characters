﻿using rpg_characters.Equipments.Weapons;
using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments
{
    //Implements IWeapon to be able to polymorph as a IWeapon in WeaponContext
    public class RangedWeapon : IWeapon
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int BaseDamage { get; set; }

        public RangedWeapon(string name, int level)
        {
            Name = name;
            Level = level;
            BaseDamage = 5;
        }
        //Calculates attackdamage depending on the stat dexterity
        public int CalculateAttackDamage(BaseStats stats)
        {
            return BaseDamage + (stats.dexterity * 2);
        }

        // Scales the basedamage to weapon lvl
        public void CalculateBaseDamage()
        {
            BaseDamage = BaseDamage + (3 * Level);
        }
    }
}
