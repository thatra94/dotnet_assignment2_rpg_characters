﻿using rpg_characters.Equipments.Weapons;
using rpg_characters.Stats;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpg_characters.Equipments
{
    //Implements IWeapon to be able to polymorph as a IWeapon in WeaponContext
    public class MeleeWeapon : IWeapon
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int BaseDamage { get; set; }

        public MeleeWeapon(string name, int level)
        {
            Name = name;
            Level = level;
            BaseDamage = 15;
        }

        //Calculates attackdamage depending on the stat strength

        public int CalculateAttackDamage(BaseStats stats)
        {
            return (int)(BaseDamage + (stats.strength * 1.5));
        }
        // Scales the basedamage to weapon lvl

        public void CalculateBaseDamage()
        {
            BaseDamage = BaseDamage + (2 * Level);
        }
    }
}
