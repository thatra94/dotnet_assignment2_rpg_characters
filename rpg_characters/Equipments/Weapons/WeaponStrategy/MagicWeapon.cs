﻿using rpg_characters.Equipments.Weapons;
using rpg_characters.Stats;

namespace rpg_characters.Equipments
{
    //Implements IWeapon to be able to polymorph as a IWeapon in WeaponContext
    public class MagicWeapon : IWeapon
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int BaseDamage { get; set; }

        public MagicWeapon(string name, int level)
        {
            Name = name;
            Level = level;
            BaseDamage = 25;
        }

        //Calculates attackdamage depending on the stat intelligence
        public int CalculateAttackDamage(BaseStats stats)
        {
            return BaseDamage + (stats.intelligence * 3);
        }

        // Scales the basedamage to weapon lvl
        public void CalculateBaseDamage()
        {
            BaseDamage = BaseDamage + (2 * Level);
        }
    }
}

