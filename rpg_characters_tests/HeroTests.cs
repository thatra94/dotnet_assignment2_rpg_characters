using System;
using Xunit;
using rpg_characters.Heroes;
using rpg_characters.Equipments;
using rpg_characters.Equipments.Armors.ArmorFactoryClient;
using rpg_characters.Equipments.Armors.LevelScaleStrategy;
using rpg_characters.Equipments.Armors.ArmorContext;
using rpg_characters.Equipments.Weapons;
using rpg_characters.Heroes.HeroStrategyClasses;

namespace rpg_characters_tests
{
    public class HeroTests
    {
        [Theory]
        [InlineData(HeroClassEnum.HeroClass.Warrior, 150, 10, 3, 1)]
        [InlineData(HeroClassEnum.HeroClass.Ranger, 120, 5, 10, 2)]
        [InlineData(HeroClassEnum.HeroClass.Mage, 100, 2, 3, 10)]
        public void Herofactory_CreatesHeroes(HeroClassEnum.HeroClass heroClass, int health, int strength, int dexterity, int intelligence)
        {
            Hero hero = HeroFactory.CreateHero(heroClass, new HeroStatsCalculations());

            Assert.Equal(health, hero.HeroClass.BaseStats.health);
            Assert.Equal(strength, hero.HeroClass.BaseStats.strength);
            Assert.Equal(dexterity, hero.HeroClass.BaseStats.dexterity);
            Assert.Equal(intelligence, hero.HeroClass.BaseStats.intelligence);
        }

        [Theory]
        [InlineData(EquipmentEnum.ArmorTypes.Plate, 270, 28, 11, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Leather, 230, 18, 21, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Cloth, 198, 10, 11, 19)]
        public void Hero_AddHeadArmorShouldScaleHeroStats(EquipmentEnum.ArmorTypes armorTypes, int effectiveHealth, int effectiveStrength, int effectiveDexterity, int effectiveIntelligence)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            Armor armor = ArmorFactory.CreateArmor("Greatarmor of the wolf", 10, armorTypes, EquipmentEnum.ArmorSlots.Head, new ScaleHeadArmor());
            hero.addEquipment(armor);

            Assert.Equal(effectiveHealth, hero.EffectiveStats.health);
            Assert.Equal(effectiveStrength, hero.EffectiveStats.strength);
            Assert.Equal(effectiveDexterity, hero.EffectiveStats.dexterity);
            Assert.Equal(effectiveIntelligence, hero.EffectiveStats.intelligence);
        }

        [Theory]
        [InlineData(EquipmentEnum.ArmorTypes.Plate, 300, 33, 14, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Leather, 250, 21, 26, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Cloth, 210, 10, 14, 24)]
        public void Hero_AddBodyArmorShouldScaleHeroStats(EquipmentEnum.ArmorTypes armorTypes, int effectiveHealth, int effectiveStrength, int effectiveDexterity, int effectiveIntelligence)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            Armor armor = ArmorFactory.CreateArmor("Greatarmor of the wolf", 10, armorTypes, EquipmentEnum.ArmorSlots.Body, new ScaleBodyArmor());
            hero.addEquipment(armor);

            Assert.Equal(effectiveHealth, hero.EffectiveStats.health);
            Assert.Equal(effectiveStrength, hero.EffectiveStats.strength);
            Assert.Equal(effectiveDexterity, hero.EffectiveStats.dexterity);
            Assert.Equal(effectiveIntelligence, hero.EffectiveStats.intelligence);
        }

        [Theory]
        [InlineData(EquipmentEnum.ArmorTypes.Plate, 240, 23, 9, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Leather, 210, 16, 16, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Cloth, 186, 10, 9, 14)]
        public void Hero_AddLegArmorShouldScaleHeroStats(EquipmentEnum.ArmorTypes armorTypes, int effectiveHealth, int effectiveStrength, int effectiveDexterity, int effectiveIntelligence)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            Armor armor = ArmorFactory.CreateArmor("Greatarmor of the wolf", 10, armorTypes, EquipmentEnum.ArmorSlots.Legs, new ScaleLegArmor());
            hero.addEquipment(armor);

            Assert.Equal(effectiveHealth, hero.EffectiveStats.health);
            Assert.Equal(effectiveStrength, hero.EffectiveStats.strength);
            Assert.Equal(effectiveDexterity, hero.EffectiveStats.dexterity);
            Assert.Equal(effectiveIntelligence, hero.EffectiveStats.intelligence);
        }

        [Theory]
        [InlineData(EquipmentEnum.ArmorTypes.Plate, 150, 10, 3, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Leather, 150, 10, 3, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Cloth, 150, 10, 3, 1)]
        public void Hero_RemoveArmorShouldScaleHeroStats(EquipmentEnum.ArmorTypes armorTypes, int effectiveHealth, int effectiveStrength, int effectiveDexterity, int effectiveIntelligence)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            Armor armor = ArmorFactory.CreateArmor("Greatarmor of the wolf", 10, armorTypes, EquipmentEnum.ArmorSlots.Head, new ScaleHeadArmor());
            hero.addEquipment(armor);
            hero.RemoveEquipment(EquipmentEnum.ArmorSlots.Head);

            Assert.Equal(effectiveHealth, hero.EffectiveStats.health);
            Assert.Equal(effectiveStrength, hero.EffectiveStats.strength);
            Assert.Equal(effectiveDexterity, hero.EffectiveStats.dexterity);
            Assert.Equal(effectiveIntelligence, hero.EffectiveStats.intelligence);
        }

        [Theory]
        [InlineData(EquipmentEnum.ArmorTypes.Plate, 270, 28, 11, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Leather, 230, 18, 21, 1)]
        [InlineData(EquipmentEnum.ArmorTypes.Cloth, 198, 10, 11, 19)]
        public void Hero_ReplaceArmorShouldScaleHeroStats(EquipmentEnum.ArmorTypes armorTypes, int effectiveHealth, int effectiveStrength, int effectiveDexterity, int effectiveIntelligence)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            Armor armor = ArmorFactory.CreateArmor("Greatarmor of the wolf", 100, armorTypes, EquipmentEnum.ArmorSlots.Head, new ScaleHeadArmor());
            Armor armorReplace = ArmorFactory.CreateArmor("Greatarmor of the wolf", 10, armorTypes, EquipmentEnum.ArmorSlots.Head, new ScaleHeadArmor());
            hero.addEquipment(armor);
            hero.addEquipment(armorReplace);

            Assert.Equal(effectiveHealth, hero.EffectiveStats.health);
            Assert.Equal(effectiveStrength, hero.EffectiveStats.strength);
            Assert.Equal(effectiveDexterity, hero.EffectiveStats.dexterity);
            Assert.Equal(effectiveIntelligence, hero.EffectiveStats.intelligence);
        }

        [Theory]
        [InlineData(EquipmentEnum.WeaponTypes.MeleeWeapon, 50)]
        [InlineData(EquipmentEnum.WeaponTypes.RangedWeapon, 41)]
        [InlineData(EquipmentEnum.WeaponTypes.MagicWeapon, 48)]
        public void Hero_AddWeaponShouldScaleHeroAttackDamage(EquipmentEnum.WeaponTypes weaponType, int heroAttackDamage)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            Weapon weapon = WeaponFactory.CreateWeapon(weaponType, "Greataxe of the lions", 10);
            hero.AddWeapon(weapon);
            Assert.Equal(heroAttackDamage, hero.AttackDamage);
        }


        [Theory]
        [InlineData(1, 10, 150, 10, 3, 1)]
        [InlineData(2, 100, 180, 15, 5, 2)]
        [InlineData(3, 210, 210, 20, 7, 3)]
        [InlineData(3, 250, 210, 20, 7, 3)]
        [InlineData(13, 3000, 510, 70, 27, 13)]
        public void Hero_ExperienceShouldLevelHeroUpAndScaleStats(int level, int experience, int health, int strength, int dexterity, int intelligence)
        {
            Hero hero = HeroFactory.CreateHero(HeroClassEnum.HeroClass.Warrior, new HeroStatsCalculations());
            hero.addExperience(experience);
            Assert.Equal(level, hero.HeroClass.BaseStats.level);
            Assert.Equal(health, hero.HeroClass.BaseStats.health);
            Assert.Equal(strength, hero.HeroClass.BaseStats.strength);
            Assert.Equal(dexterity, hero.HeroClass.BaseStats.dexterity);
            Assert.Equal(intelligence, hero.HeroClass.BaseStats.intelligence);
        }

    }
}
